---
title: "Контакты"
logo: "/img/logo.svg"
contact_entries:
  - heading: Место
    text: "Москва, Покровский бульвар"
  - heading: Время
    text: "Каждый четверг"
---

Пишите. Мы будем рады.

<h3 class="f4 b lh-title mb2">Как нас найти</h3>

Вы также можете воспользоваться приведенной ниже формой для любых вопросов.
Не стесняйтесь, пишите нам!
